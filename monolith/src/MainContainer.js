import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
class MainContainer extends Component {
    componentDidUpdate(prevProps){
        if(this.props.location.pathname === prevProps.location.pathname)
        window.scrollTo(0,0)
        console.log(this.props.location.pathname, prevProps.location.pathname)
    }
  render() {
    return <main>{this.props.children}</main>
  }
}

export default withRouter(MainContainer)