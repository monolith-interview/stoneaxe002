import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'

const {Provider, Consumer} = React.createContext();

class UserProviders extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         userId: '주인공은 나야나(나)', 
         freinds: [
            '주인공은 나야나(나)',
            '내 친구1',
            '내 친구2',
            '내 친구3'
         ],
         freindsE: [
            '주인공은 나야나(나)',
            '내 친구1(E)',
            '내 친구2(E)',
            '내 친구3(E)'
         ],
         freindsD3: [
            '주인공은 나야나(나)',
            '내 친구1(D3)',
            '내 친구2(D3)',
            '내 친구3(D3)'
         ],
         freindsX4L: [
            '주인공은 나야나(나)',
            '내 친구1(X4L)',
            '내 친구2(X4L)',
            '내 친구3(X4L)'
         ],
         freindsX4R: [
            '주인공은 나야나(나)',
            '내 친구1(X4R)',
            '내 친구2(X4R)',
            '내 친구3(X4R)'
         ],
      }
    }
    
  render() {
    return <Provider value={this.state}>{this.props.children}</Provider>
  }
}

const UserProvider = withRouter(UserProviders)

function withUser(WrappedComponent){
    return function(props){
        return (
            <Consumer>{value=> <WrappedComponent {...value} {...props}/>}</Consumer>
        )
    }
}

export { UserProvider, Consumer as UserConsumer, withUser}