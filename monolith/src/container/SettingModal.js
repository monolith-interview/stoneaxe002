import React, { Component } from 'react'
import SettingModalView from '../component/SettingModalView';

export default class SettingModal extends Component {
  render() {
      const {onCategory, onModal, onArray} = this.props
    return (
      <div>
        <SettingModalView 
        onModal={()=>onModal()} 
        onCategory={(input)=> onCategory(input)}
        onArray={(input)=>onArray(input)}
        {...this.props}
        />
      </div>
    )
  }
}
