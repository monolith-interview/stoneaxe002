import React, { Component } from 'react'
import RankingListView from '../component/RankingListView';
import { withUser } from '../context/UserContext';
import avatar1 from '../component/img/avatar1.png'
import avatar2 from '../component/img/avatar2.png'
import avatar3 from '../component/img/avatar3.png'
import avatar4 from '../component/img/avatar4.png'
import avatar5 from '../component/img/avatar5.png'
import avatar6 from '../component/img/avatar6.png'
import avatar7 from '../component/img/avatar7.png'
import avatar8 from '../component/img/avatar8.png'
import avatar9 from '../component/img/avatar9.png'
import avatar10 from '../component/img/avatar10.png'
import avatar11 from '../component/img/avatar11.png'
import avatar12 from '../component/img/avatar12.png'
class RankingList extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         userlist: [
             {
                 id: '별이 언니_0210',
                 record: '01:08.31',
                 point: '300 point',
                 round: '1round', 
                 avatar: avatar1
             },
             {
                 id: '누나는 쪙뽀도',
                 record: '01:08.37',
                 point: '299 point',
                 round: '2round',
                 avatar: avatar2

             },
             {
                 id: '평창동오징어',
                 record: '01:09.02',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar3

             },
             {
                 id: 'tc_ki',
                 record: '01:09.11',
                 point: '280 point',
                 round: '2round',
                 avatar: avatar4

             },
             {
                 id: '문희는 포도',
                 record: '01:09.29',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar5
             },
             {
                 id: '콩콩진진호',
                 record: '01:09.48',
                 point: '260 point',
                 round: '2round',
                 avatar: avatar6
             },
             {
                 id: 'neople',
                 record: '01:09.66',
                 point: '250 point',
                 round: '2round',
                 avatar: avatar7
             },
             {
                 id: '주인공은 나야나(나)',
                 record: '01:23.45',
                 point: '240 point',
                 round: '1round',
                 avatar: avatar8
             },
             {
                id: '내 친구1',
                record: '01:30.66',
                point: '230 point',
                round: '2round',
                avatar: avatar9
             },
             {
                id: '내 친구2',
                record: '01:30.66',
                point: '220 point',
                round: '1round',
                avatar: avatar10
             },
             {
                id: '내 친구3',
                record: '01:30.66',
                point: '210 point',
                round: '2round',
                avatar: avatar11
             },
             {
                id: 'neople',
                record: '01:30.66',
                point: '200 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople',
                record: '01:30.66',
                point: '190 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople',
                record: '01:30.66',
                point: '180 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople',
                record: '01:30.66',
                point: '170 point',
                round: '1round',
                avatar: avatar12
             },
             {
                id: 'neople',
                record: '01:30.66',
                point: '160 point',
                round: '2round',
                avatar: avatar12
             },
         ],
         Elist: [
             {
                 id: '별이 언니_0210(E)',
                 record: '01:08.31',
                 point: '300 point',
                 round: '1round', 
                 avatar: avatar1
             },
             {
                 id: '누나는 쪙뽀도(E)',
                 record: '01:08.37',
                 point: '299 point',
                 round: '2round',
                 avatar: avatar2

             },
             {
                 id: '평창동오징어(E)',
                 record: '01:09.02',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar3

             },
             {
                 id: 'tc_ki(E)',
                 record: '01:09.11',
                 point: '280 point',
                 round: '2round',
                 avatar: avatar4

             },
             {
                 id: '문희는 포도(E)',
                 record: '01:09.29',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar5
             },
             {
                 id: '콩콩진진호(E)',
                 record: '01:09.48',
                 point: '260 point',
                 round: '2round',
                 avatar: avatar6
             },
             {
                 id: 'neople(E)',
                 record: '01:09.66',
                 point: '250 point',
                 round: '2round',
                 avatar: avatar7
             },
             {
                 id: '주인공은 나야나(나)',
                 record: '01:23.45',
                 point: '240 point',
                 round: '1round',
                 avatar: avatar8
             },
             {
                id: '내 친구1(E)',
                record: '01:30.66',
                point: '230 point',
                round: '2round',
                avatar: avatar9
             },
             {
                id: '내 친구2(E)',
                record: '01:30.66',
                point: '220 point',
                round: '1round',
                avatar: avatar10
             },
             {
                id: '내 친구3(E)',
                record: '01:30.66',
                point: '210 point',
                round: '2round',
                avatar: avatar11
             },
             {
                id: 'neople(E)',
                record: '01:30.66',
                point: '200 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(E)',
                record: '01:30.66',
                point: '190 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(E)',
                record: '01:30.66',
                point: '180 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(E)',
                record: '01:30.66',
                point: '170 point',
                round: '1round',
                avatar: avatar12
             },
             {
                id: 'neople(E)',
                record: '01:30.66',
                point: '160 point',
                round: '2round',
                avatar: avatar12
             },
         ],
         D3list: [
             {
                 id: '별이 언니_0210(D3)',
                 record: '01:08.31',
                 point: '300 point',
                 round: '1round', 
                 avatar: avatar1
             },
             {
                 id: '누나는 쪙뽀도(D3)',
                 record: '01:08.37',
                 point: '299 point',
                 round: '2round',
                 avatar: avatar2

             },
             {
                 id: '평창동오징어(D3)',
                 record: '01:09.02',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar3

             },
             {
                 id: 'tc_ki(D3)',
                 record: '01:09.11',
                 point: '280 point',
                 round: '2round',
                 avatar: avatar4

             },
             {
                 id: '문희는 포도(D3)',
                 record: '01:09.29',
                 point: '290 point',
                 round: '1round',
                 avatar: avatar5
             },
             {
                 id: '콩콩진진호(D3)',
                 record: '01:09.48',
                 point: '260 point',
                 round: '2round',
                 avatar: avatar6
             },
             {
                 id: 'neople(D3)',
                 record: '01:09.66',
                 point: '250 point',
                 round: '2round',
                 avatar: avatar7
             },
             {
                 id: '주인공은 나야나(나)',
                 record: '01:23.45',
                 point: '240 point',
                 round: '1round',
                 avatar: avatar8
             },
             {
                id: '내 친구1(D3)',
                record: '01:30.66',
                point: '230 point',
                round: '2round',
                avatar: avatar9
             },
             {
                id: '내 친구2(D3)',
                record: '01:30.66',
                point: '220 point',
                round: '1round',
                avatar: avatar10
             },
             {
                id: '내 친구3(D3)',
                record: '01:30.66',
                point: '210 point',
                round: '2round',
                avatar: avatar11
             },
             {
                id: 'neople(D3)',
                record: '01:30.66',
                point: '200 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(D3)',
                record: '01:30.66',
                point: '190 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(D3)',
                record: '01:30.66',
                point: '180 point',
                round: '2round',
                avatar: avatar12
             },
             {
                id: 'neople(D3)',
                record: '01:30.66',
                point: '170 point',
                round: '1round',
                avatar: avatar12
             },
             {
                id: 'neople(D3)',
                record: '01:30.66',
                point: '160 point',
                round: '2round',
                avatar: avatar12
             },
         ],
         X4Llist: [
            {
                id: '별이 언니_0210(X4L)',
                record: '01:08.31',
                point: '300 point',
                round: '1round', 
                avatar: avatar1
            },
            {
                id: '누나는 쪙뽀도(X4L)',
                record: '01:08.37',
                point: '299 point',
                round: '2round',
                avatar: avatar2

            },
            {
                id: '평창동오징어(X4L)',
                record: '01:09.02',
                point: '290 point',
                round: '1round',
                avatar: avatar3

            },
            {
                id: 'tc_ki(X4L)',
                record: '01:09.11',
                point: '280 point',
                round: '2round',
                avatar: avatar4

            },
            {
                id: '문희는 포도(X4L)',
                record: '01:09.29',
                point: '290 point',
                round: '1round',
                avatar: avatar5
            },
            {
                id: '콩콩진진호(X4L)',
                record: '01:09.48',
                point: '260 point',
                round: '2round',
                avatar: avatar6
            },
            {
                id: 'neople(X4L)',
                record: '01:09.66',
                point: '250 point',
                round: '2round',
                avatar: avatar7
            },
            {
                id: '주인공은 나야나(나)',
                record: '01:23.45',
                point: '240 point',
                round: '1round',
                avatar: avatar8
            },
            {
               id: '내 친구1(X4L)',
               record: '01:30.66',
               point: '230 point',
               round: '2round',
               avatar: avatar9
            },
            {
               id: '내 친구2(X4L)',
               record: '01:30.66',
               point: '220 point',
               round: '1round',
               avatar: avatar10
            },
            {
               id: '내 친구3(X4L)',
               record: '01:30.66',
               point: '210 point',
               round: '2round',
               avatar: avatar11
            },
            {
               id: 'neople(X4L)',
               record: '01:30.66',
               point: '200 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4L)',
               record: '01:30.66',
               point: '190 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4L)',
               record: '01:30.66',
               point: '180 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4L)',
               record: '01:30.66',
               point: '170 point',
               round: '1round',
               avatar: avatar12
            },
            {
               id: 'neople(X4L)',
               record: '01:30.66',
               point: '160 point',
               round: '2round',
               avatar: avatar12
            },
        ],
        X4Rlist: [
            {
                id: '별이 언니_0210(X4R)',
                record: '01:08.31',
                point: '300 point',
                round: '1round', 
                avatar: avatar1
            },
            {
                id: '누나는 쪙뽀도(X4R)',
                record: '01:08.37',
                point: '299 point',
                round: '2round',
                avatar: avatar2

            },
            {
                id: '평창동오징어(X4R)',
                record: '01:09.02',
                point: '290 point',
                round: '1round',
                avatar: avatar3

            },
            {
                id: 'tc_ki(X4R)',
                record: '01:09.11',
                point: '280 point',
                round: '2round',
                avatar: avatar4

            },
            {
                id: '문희는 포도(X4R)',
                record: '01:09.29',
                point: '290 point',
                round: '1round',
                avatar: avatar5
            },
            {
                id: '콩콩진진호(X4R)',
                record: '01:09.48',
                point: '260 point',
                round: '2round',
                avatar: avatar6
            },
            {
                id: 'neople(X4R)',
                record: '01:09.66',
                point: '250 point',
                round: '2round',
                avatar: avatar7
            },
            {
                id: '주인공은 나야나(나)',
                record: '01:23.45',
                point: '240 point',
                round: '1round',
                avatar: avatar8
            },
            {
               id: '내 친구1(X4R)',
               record: '01:30.66',
               point: '230 point',
               round: '2round',
               avatar: avatar9
            },
            {
               id: '내 친구2(X4R)',
               record: '01:30.66',
               point: '220 point',
               round: '1round',
               avatar: avatar10
            },
            {
               id: '내 친구3(X4R)',
               record: '01:30.66',
               point: '210 point',
               round: '2round',
               avatar: avatar11
            },
            {
               id: 'neople(X4R)',
               record: '01:30.66',
               point: '200 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4R)',
               record: '01:30.66',
               point: '190 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4R)',
               record: '01:30.66',
               point: '180 point',
               round: '2round',
               avatar: avatar12
            },
            {
               id: 'neople(X4R)',
               record: '01:30.66',
               point: '170 point',
               round: '1round',
               avatar: avatar12
            },
            {
               id: 'neople(X4R)',
               record: '01:30.66',
               point: '160 point',
               round: '2round',
               avatar: avatar12
            },
        ],
      }
    }
    createUserList(array, freinds, userList){
        array.map(user=>(
            freinds.map(freind=>(
                  user.id === freind ?(
                    userList.push(user)
                  ):null
              ))
          ))
        return userList
    }
  render() {
      const userList = []
      const userList2 = []
      const userList3 = []
      const userList4 = []
      const userList5 = []
      const {userlist, Elist, D3list, X4Llist, X4Rlist} = this.state
      const {freinds, freindsE, freindsD3, freindsX4L, freindsX4R} = this.props
    return (
      <div>
        <RankingListView 
        Meref={this.props.innerRef} 
        Round1Ref={this.props.innerRef2} 
        Round2Ref={this.props.innerRef3} 
        userList={this.createUserList(userlist, freinds, userList)}
        userList2={this.createUserList(Elist, freindsE, userList2)}
        userList3={this.createUserList(D3list, freindsD3, userList3)}
        userList4={this.createUserList(X4Llist, freindsX4L, userList4)}
        userList5={this.createUserList(X4Rlist, freindsX4R, userList5)}
        {...this.props} 
        {...this.state}
        />
      </div>
    )
  }
}

export default withUser(RankingList)