import React, { Component } from 'react'
import RankingView from '../component/RankingView';

export default class Ranking extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         modalclick: false,
         course: 'E1',
         rankcategory: '실시간 Race',
         arraytype: '전체랭킹',
         totalchecked: true,
         friendchecked: false
      }
    }
    handleModal(){
        this.setState({
            modalclick: this.state.modalclick === false? true: false
        })
    }
    handleCategory(input){
        this.setState({
            rankcategory: input
        })
    }
    handleArray(input){
        this.setState({
            arraytype: input,
            totalchecked: this.state.totalchecked === true? false: true,
            friendchecked: this.state.friendchecked === true? false: true
        })
    }
    handleCourse(input){
        this.setState({
            course: input
        })
    }
  render() {
    return (
      <div>
        <RankingView 
        {...this.state} 
        onModal={()=>this.handleModal()}
        onCategory={(input)=>this.handleCategory(input)}
        onArray={(input)=>this.handleArray(input)}
        onCourse={(input)=>this.handleCourse(input)}
        />
      </div>
    )
  }
}
