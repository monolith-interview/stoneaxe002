import React, { Component } from 'react';
import './App.css';
import RankingPage from './page/RankingPage';
import {UserProvider} from './context/UserContext';
import { BrowserRouter} from 'react-router-dom'
import MainContainer from './MainContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
      <BrowserRouter>
        <UserProvider>
          <MainContainer>
            <RankingPage/>
          </MainContainer>
        </UserProvider>
      </BrowserRouter>
      </div>
    );
  }
}

export default App;
