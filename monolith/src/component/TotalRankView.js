import React, { Component } from 'react'
import style from './RankingListView.module.scss'
import { ReactComponent as Medal } from '../svg/champ.svg';

export default class TotalRankView extends Component {
  render() {
    const {userlist, Elist, D3list, X4Llist, X4Rlist, userId, 
      arraytype, Meref, userList, userList2, userList3, userList4, 
      userList5, course} = this.props
    return (
        arraytype === '전체랭킹'?(
            (course === 'E1'?userlist
            :course ==='E'?Elist
            :course==='D3'?D3list
            :course==='X4(L'?X4Llist
            :course==='X4(R'?X4Rlist
            :null).map((user, index)=>(
              <li className={style.userItem} key={index}
              ref={user.id === userId? Meref: null}
              >
                {
                  index === 0?(
                    <Medal className={style.icon}/>
                  ):null
                }
                <span className={style.rank}>{index+1}</span>
                <div className={style.img}
                  style={{
                    backgroundImage: `url(${user.avatar})`
                  }}
                ></div>
                <dl className={style.userInfo}>
                  <dt>{user.id}</dt>
                  <dd>{user.record}</dd>
                </dl>
              </li>
            ))
          ): (
            (course==='E1'?userList
            :course==='E'?userList2
            :course ==='D3'?userList3
            :course==='X4(L'?userList4
            :course==='X4(R'?userList5
            :null).map((user, index1)=>(
                <li className={style.userItem} key={index1}
                  ref={user.id === userId? Meref: null}
                >
                {
                  index1 === 0?(
                    <Medal className={style.icon}/>
                  ):null
                }
                <span className={style.rank}>{index1+1}</span>
                <div className={style.img}
                    style={{
                    backgroundImage: `url(${user.avatar})`
                    }}
                ></div>
                <dl className={style.userInfo}>
                    <dt>{user.id}</dt>
                    <dd>{user.record}</dd>
                </dl>
                </li>
            ))
          )
    )
  }
}
