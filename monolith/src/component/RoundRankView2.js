import React, { Component } from 'react'
import { ReactComponent as Medal } from '../svg/champ.svg';
import style from './RankingListView.module.scss'
export default class RoundRankView extends Component {
  render() {
    const {RoundList, course, userId, arraytype, Meref, Round2Ref} = this.props
    const freindList = []
    this.props.RoundList.round2.map(round2=>(        
      (course === 'E1'?this.props.freinds: 
      course === 'E'? this.props.freindsE:
      course === 'D3'? this.props.freindsD3:
      course === 'X4(L'? this.props.freindsX4L:
      course === 'X4(R'? this.props.freindsX4R:
      null).map(freind=>(
        round2.id === freind?(
          freindList.push(round2)
          ):null
      ))
    ))
    return (
      <div>
        {
          arraytype === '전체랭킹'?(
              RoundList.round2.map((user, index)=>(
                <li className={style.userItem} key={index}
                  ref={index === 0? Round2Ref:user.id === userId? Meref:null}
                >
                   {
                    index === 0?(
                      <Medal className={style.icon}/>
                    ):null
                    }
                  <span className={style.rank}>{index+1}</span>
                  <div className={style.img}
                        style={{
                          backgroundImage: `url(${user.avatar})`
                        }}                    
                  ></div>
                  <dl className={style.userInfo}>
                    <dt className={style.round}>{user.id}</dt>
                    <dd className={style.round}>{user.round}</dd>
                    <dd className={style.round}>{user.record}</dd>
                  </dl>
                </li>
              )) 
            ): (
              freindList.map((user, index1)=>(
                  <li className={style.userItem} key={index1}
                  ref={index1 === 0? Round2Ref:user.id === userId? Meref:null}
                  >
                 {
                    index1 === 0?(
                      <Medal className={style.icon}/>
                    ):null
                  }
                  <span className={style.rank}>{index1+1}</span>
                  <div className={style.img}
                    style={{
                      backgroundImage: `url(${user.avatar})`
                    }}                        
                  ></div>
                  <dl className={style.userInfo}>
                    <dt className={style.round}>{user.id}</dt>
                    <dd className={style.round}>{user.round}</dd>
                    <dd className={style.round}>{user.record}</dd>
                  </dl>
                </li>
              ))
            )
        }
      </div>
    )
  }
}
