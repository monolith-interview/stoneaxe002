import React, { Component } from 'react'
import style from './SettingModalView.module.scss'
import { ReactComponent as Live } from '../svg/live.svg';
import { ReactComponent as Point } from '../svg/point.svg';
import { ReactComponent as Round } from '../svg/round.svg';
import { ReactComponent as Champ } from '../svg/champ.svg';

export default class SettingModalView extends Component {
    handleApply(){
        this.props.onModal()
    }
  render() {
      const {onCategory, onArray, totalchecked, friendchecked } = this.props
      return (
      <div className={style.setting}>
        <h1 className={style.title}>설정</h1>
        <label className={style.subTitle} htmlFor={style.arrayList}>기본정렬</label>
        <ul className={style.arrayList}>
            <li className={style.arrayItem}>
                <input onChange={input=>onArray('전체랭킹')} type="checkbox" checked={totalchecked}/> 전체랭킹
            </li>
            <li onClick={input=>onArray('친구만')}className={style.arrayItem}>
                <input onChange={input=>onArray('친구만')} type="checkbox" checked={friendchecked}/> 친구만
            </li>
        </ul>
        <label className={style.subTitle} htmlFor={style.typeList}>랭킹 분류</label>
        <ul className={style.typeList}>
            <li className={style.typeItem}>
                <button onClick ={(input)=>onCategory('실시간 Race')} className={style.typeBtn}>
                <Live className={style.live}/>
                LIVE</button>
                <label htmlFor={style.typeBtn}>실시간 RACE</label>
            </li>
            <li className={style.typeItem}>
                <button onClick ={(input)=>onCategory('POINT 별 Race')} className={style.typeBtn}>
                <Point className={style.live}/>
                POINT</button>
                <label htmlFor={style.typeBtn}>POINT 별 RACE</label>
            </li>
            <li className={style.typeItem}>
                <button onClick ={(input)=>onCategory('ROUND 별')} className={style.typeBtn}>
                <Round className={style.live}/>
                ROUND</button>
                <label htmlFor={style.typeBtn}>ROUND 별</label>
            </li>
            <li className={style.typeItem}>
                <button onClick ={(input)=>onCategory('CHAMPION')} className={style.typeBtn}>
                <Champ className={style.live}/>
                CHAMP</button>
                <label htmlFor={style.typeBtn}>CHAMPION</label>
            </li>
        </ul>
        <button onClick={()=>this.handleApply()} className={style.applyBtn}>적용</button>
      </div>
    )
  }
}
