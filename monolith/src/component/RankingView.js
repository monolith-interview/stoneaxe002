import React, { Component } from 'react'
import style from './RankingView.module.scss'
import RankingList from '../container/RankingList';
import SettingModal from '../container/SettingModal';
import { ReactComponent as ArrowRight } from '../svg/arrowRight.svg';
import { ReactComponent as Car } from '../svg/car.svg';
import back1 from './img/back1.png'
export default class RankingView extends Component {
    constructor(props) {
        super(props)
        this.MeRef = React.createRef();
        this.Round1Ref = React.createRef();
        this.Round2Ref = React.createRef()
        this.state = {
            myrank: false,
            coursecategory: ['E1', 'E', 'D3', 'X4(L', 'X4(R']
        }
    }
    handleSroll(){
        const currentScroll = window.scrollY;
        if(this.state.myrank === false && this.MeRef.current === null){
            alert('랭킹에 속하지 않았습니다.')
        }else if(this.state.myrank === false){
            window.scroll(0, currentScroll + this.MeRef.current.getBoundingClientRect().y - 120)
            this.setState({
                myrank: this.state.myrank === false?true: false
            })    
        }else{
            window.scroll(0,0)
            this.setState({
                myrank: this.state.myrank === false?true: false
            })    
        }
    }
    handleRound1(){
        const currentScroll = window.scrollY;
        const Round1Location = this.Round1Ref.current.getBoundingClientRect();
        window.scroll(0, currentScroll + Round1Location.y - 120);
    }
    handleRound2(){
        const currentScroll = window.scrollY;
        const Round2Location = this.Round2Ref.current.getBoundingClientRect();
        window.scroll(0, currentScroll + Round2Location.y - 120);
    }
    render() {
      const {modalclick, onModal, rankcategory, onCategory, onArray, onCourse, course} = this.props
        const {myrank, coursecategory} = this.state
    return (
      <div className={style.rankingView}>
        <section className={style.rankingHead}>
                <h1 className={style.title}>RANKING</h1>
            <article className={style.pageTitle}>
                <Car className={style.car}/>
                <label className={style.type} htmlFor={style.rankingBtn}>랭킹 타입</label>
                <button onClick={()=>onModal()} className={style.rankingBtn}>
                {rankcategory}
                </button>
                <div 
                className={style.myRank}>
                <ArrowRight className={style.arrow}/>
                </div> 
            </article>
            <article>
                <ul className={style.raceList}>
                    <li className={style.raceItem}> 
                        <button
                        onClick={()=>this.handleSroll()}
                        style={this.state.myrank===true?{
                            backgroundImage: `url(${back1})`,
                            color: '#12341c',
                        }:null} 
                        >{myrank===false?'My Rank':'Top'}</button>
                    </li>
                    {
                        coursecategory.map((course2, index)=>(
                            <li className={style.raceItem} key={index}> 
                                <button 
                                onClick={(input)=>onCourse(course2)}
                                style={course===course2?{
                                    backgroundImage: `url(${back1})`,
                                    color: '#12341c',
                                }:null}>
                                {course2}</button>
                            </li>
                        ))
                    }
                </ul>
            </article>
        </section>
        <section>
            <RankingList 
            innerRef={this.MeRef}
            innerRef2 = {this.Round1Ref}
            innerRef3 = {this.Round2Ref}
            onRound1={()=>this.handleRound1()}
            onRound2={()=>this.handleRound2()}
            {...this.props}
            />
        </section>
        {
            modalclick === true?(
                <section>
                    <SettingModal 
                    onModal={()=>onModal()} 
                    onCategory={(input)=>onCategory(input)}
                    onArray={(input)=>onArray(input)}
                    {...this.props}
                    />
                </section>
            ):null
        }
      </div>
    )
  }
}
