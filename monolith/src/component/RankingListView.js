import React, { Component } from 'react'
import { withUser } from '../context/UserContext';
import CourseItem from './CourseItem';


class RankingListView extends Component {
  createRoundList(array, RoundList){
    array.map(user=>(
        user.round === '1round'?(
            RoundList.round1.push(user)
        ):(
            RoundList.round2.push(user)
        )
    ))
    return RoundList
  }
  render() {
    const { userlist, Elist, D3list, X4Llist, X4Rlist, course} = this.props
    const RoundList = {round1:[], round2: []}
    const RoundList2 = {round1:[], round2: []}
    const RoundList3 = {round1:[], round2: []}
    const RoundList4 = {round1:[], round2: []}
    const RoundList5 = {round1:[], round2: []}
    return (
      <div>
        {
          course === 'E1'?(
            <CourseItem {...this.props} RoundList={this.createRoundList(userlist, RoundList)}/>
          ):course === 'E'?(
            <CourseItem {...this.props} RoundList={this.createRoundList(Elist, RoundList2)}/>
          ):course === 'D3'?(
            <CourseItem {...this.props} RoundList={this.createRoundList(D3list, RoundList3)}/>
          ):course === 'X4(L'?(
            <CourseItem {...this.props} RoundList={this.createRoundList(X4Llist, RoundList4)}/>
          ):course === 'X4(R'?(
            <CourseItem {...this.props} RoundList={this.createRoundList(X4Rlist, RoundList5)}/>
          ):null
        }
      </div>
    )
  }
}
export default withUser(RankingListView)