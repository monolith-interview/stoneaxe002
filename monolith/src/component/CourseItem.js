import React, { Component } from 'react'
import style from './RankingListView.module.scss'
import TotalRankView from './TotalRankView';
import PointRankView from './PointRankView';
import RoundRankView from './RoundRankView';
import RoundRankView2 from './RoundRankView2';
import ChampionView from './ChampionView';

export default class CourseItem extends Component {
  render() {
    const {rankcategory} = this.props
    return (
        <ul className={style.userList}>
        {
          rankcategory === '실시간 Race'?(
            <TotalRankView {...this.props}/>
          ):rankcategory === 'POINT 별 Race'?(
            <PointRankView {...this.props}/>
          ):rankcategory === 'ROUND 별'?(
            <div>
              <ul className={style.roundBtnList}>
                <li className={style.roundBtnItem}>
                <button onClick={()=>this.props.onRound1()}>1 ROUND</button></li>
                <li className={style.roundBtnItem}>
                <button onClick={()=>this.props.onRound2()}>2 ROUND</button></li>
              </ul>
              <RoundRankView {...this.props}/>
              <RoundRankView2 {...this.props}/>
            </div>
          ): rankcategory === 'CHAMPION'?(
            <ChampionView {...this.props}/>
          ):null
        }
      </ul>
)
  }
}
