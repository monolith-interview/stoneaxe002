import React, { Component } from 'react'
import { ReactComponent as Medal } from '../svg/champ.svg';
import style from './RankingListView.module.scss'

export default class ChampionView extends Component {
  render() {
    const { userlist, arraytype, course, userList, userList2, userList3, 
        Elist, D3list, X4Llist, X4Rlist, userList4, userList5, Meref, userId} = this.props
    return (
        arraytype === '전체랭킹'?(
            (course === 'E1'?userlist
            :course ==='E'?Elist
            :course==='D3'?D3list
            :course==='X4(L'?X4Llist
            :course==='X4(R'?X4Rlist
            :null).map((user, index)=>(
                index === 0?(
                    <li className={style.userItem} key={index}
                    ref={user.id === userId?Meref:null}>
                    <Medal className={style.icon}/>
                    <span className={style.rank}>1</span>
                    <div className={style.img}
                    style={{
                        backgroundImage: `url(${user.avatar})`
                    }}              
                    ></div>
                    <dl className={style.userInfo}>
                    <dt className={style.round}>{user.id}</dt>
                    <dd className={style.round}>{user.record}</dd>
                    <dd className={style.round}>{user.point}</dd>
                    </dl>
                    </li>
                ):null
            ))
        ):(
            <li className={style.userItem} ref={userList[0].id === userId?Meref:null}>
            <Medal className={style.icon}/>
            <span className={style.rank}>1</span>
            <div className={style.img}
            style={{backgroundImage: `url(${userList[0].avatar})`}}              
            ></div>
            {
            course === 'E1'?(
                <dl className={style.userInfo}>
                <dt className={style.round}>{userList[0].id}</dt>
                <dd className={style.round}>{userList[0].record}</dd>
                <dd className={style.round}>{userList[0].point}</dd>
                </dl>        
            ):course === 'E'?(
                <dl className={style.userInfo}>
                <dt className={style.round}>{userList2[0].id}</dt>
                <dd className={style.round}>{userList2[0].record}</dd>
                <dd className={style.round}>{userList2[0].point}</dd>
                </dl>        
            ): course === 'D3'?(
                <dl className={style.userInfo}>
                <dt className={style.round}>{userList3[0].id}</dt>
                <dd className={style.round}>{userList3[0].record}</dd>
                <dd className={style.round}>{userList3[0].point}</dd>
                </dl>        
            ): course === 'X4(L'?(
                <dl className={style.userInfo}>
                <dt className={style.round}>{userList4[0].id}</dt>
                <dd className={style.round}>{userList4[0].record}</dd>
                <dd className={style.round}>{userList4[0].point}</dd>
                </dl>  
            ): course === 'X4(R'?(
                <dl className={style.userInfo}>
                <dt className={style.round}>{userList5[0].id}</dt>
                <dd className={style.round}>{userList5[0].record}</dd>
                <dd className={style.round}>{userList5[0].point}</dd>
                </dl>  
            ):null}
            </li>
        )
    )
  }
}
