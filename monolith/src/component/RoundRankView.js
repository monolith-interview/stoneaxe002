import React, { Component } from 'react'
import { ReactComponent as Medal } from '../svg/champ.svg';
import style from './RankingListView.module.scss'
export default class RoundRankView extends Component {
  render() {
    const {RoundList, course, userId, arraytype, Meref, Round1Ref} = this.props
    const freindList = []
    RoundList.round1.map(round1=>(        
      (course === 'E1'?this.props.freinds: 
      course === 'E'? this.props.freindsE:
      course === 'D3'? this.props.freindsD3:
      course === 'X4(L'? this.props.freindsX4L:
      course === 'X4(R'? this.props.freindsX4R:
      null).map(freind=>(
        round1.id === freind?(
          freindList.push(round1)
          ):null
      ))
    ))
    return (
      <div>
        {
          arraytype === '전체랭킹'?(
              RoundList.round1.map((user, index)=>(
                <li className={style.userItem} key={index}
                ref={index === 0? Round1Ref:null}
                >
                   {
                    index === 0?(
                      <Medal className={style.icon}/>
                    ):null
                    }
                  <span ref={user.id === userId? Meref:null} className={style.rank}>{index+1}</span>
                  <div className={style.img}
                        style={{
                          backgroundImage: `url(${user.avatar})`
                        }}                    
                  ></div>
                  <dl className={style.userInfo}>
                    <dt className={style.round}>{user.id}</dt>
                    <dd className={style.round}>{user.round}</dd>
                    <dd className={style.round}>{user.record}</dd>
                  </dl>
                </li>
              ))
            ): (
              freindList.map((user, index1)=>(
                <li className={style.userItem} key={index1}
                ref={index1 === 0? Round1Ref:null}                    
                >
                {
                  index1 === 0?(
                    <Medal className={style.icon}/>
                  ):null
                }
                <span ref={user.id === userId? Meref:null} className={style.rank}>{index1+1}</span>
                <div className={style.img}
                  style={{
                    backgroundImage: `url(${user.avatar})`
                  }}                        
                ></div>
                <dl className={style.userInfo}>
                  <dt className={style.round}>{user.id}</dt>
                  <dd className={style.round}>{user.round}</dd>
                  <dd className={style.round}>{user.record}</dd>
                </dl>
              </li>
              ))
            )
        }
      </div>
    )
  }
}
